import { Settings, TileSize, Levels, Player, Tokens, Weapon, resetPlayer } from "./globals";
import { getTileById } from "./spritesheet";
import Level1 from './levels/level_1';
import Level2 from './levels/level_2';
import Level3 from './levels/level_3';
import Level4 from './levels/level_4';

Levels.push(Level1, Level2, Level3, Level4);

/** all tiles that are considered walkable */
export const Walkable = [6, 7, 8, 22, 23, 24, 25, 26, 27, 28, 29, 30, 38, 39, 54, 59, 60, 61, 62, 65, 66, 67, 68, 70, 71, 82, 86, 91, 
  92, 93, 94, 99, 114, 118, 119, 120, 121, 122, 123, 124, 125, 126, 150, 151, 152, 153, 154, 155, 156, 157, 158, 161, 162, 163, 164, 
  165, 166, 167, 182, 183, 184, 185, 186, 187, 188, 189, 190, 193, 194, 195, 196, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 
  213, 217, 218, 219, 220, 221, 222, 225, 226, 227, 228, 229, 241, 242, 243, 244, 245, 249, 250, 251, 252, 253, 254, 257, 258, 259, 
  260, 264, 273, 274, 275, 276, 277, 281, 283, 284, 285, 286];

export const Interactable = [getTileById("Floor_ladder")];

export function getMapTile(map, tileX, tileY) {
  return map.tiles[tileY * map.width + tileX];
}

export function getMapWidth(map) {
  return map.width * TileSize;
}

export function getMapHeight(map) {
  return map.height * TileSize;
}

export function loadLevel(level) {
  Settings.map = level.map;
  Player.position.x = level.spawn.x;
  Player.position.y = level.spawn.y;
  Tokens.push(Player);
  Tokens.push(Weapon);

  //Tokens.push(...level.tokens); (... pushes the whole array)

  for (let i = 0; i < level.tokens.length; i++) {
      Tokens.push(level.tokens[i]);
  }
}

export function unloadCurrentLevel() {
  Tokens.splice(0, Tokens.length);
}

export function levelTransition() {
  unloadCurrentLevel();
  Settings.curLevel++;
  //TODO what happens if no levels are left?
  if (Settings.curLevel >= Levels.length) {
    Settings.curLevel = 0;
  }
  loadLevel(Levels[Settings.curLevel]);
}

export function restartGame() {
  unloadCurrentLevel();
  resetPlayer();
  loadLevel(Levels[Settings.curLevel]);
}