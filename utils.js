export function clamp(x, y, clamp) {
  const magnitude = Math.sqrt(x * x + y * y);
  if (magnitude > clamp) {
    const ratio = clamp / magnitude;
    x *= ratio;
    y *= ratio;
  }
  return { x, y };
}
export function normalize(x, y) {
  const magnitude = Math.sqrt(x * x + y * y);
  return {
    x: x / magnitude,
    y: y / magnitude
  } 
}