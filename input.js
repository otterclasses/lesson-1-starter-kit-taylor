// Handles keyboard controls
export var keysDown = {};
export var keysUp = new Set();
export let mouse = {
	click: false,
	x: null,
	y: null,
};

addEventListener("keydown", function (e) {
	keysDown[e.key] = true;
	keysUp.delete(e.key);
}, false);

addEventListener("keyup", function (e) {
	delete keysDown[e.key];
	keysUp.add(e.key);
}, false);

export function keysUpReset() {
	keysUp.clear();
}

export function getController() {
	let controllers = navigator.getGamepads();
	for (let x = 0; x < controllers.length; x++) {
		if (controllers[x] != null) {
			return controllers[x];
		}
	}
	return null;
}

function mouseDown(e) {
	console.log("An event happened", e);
	mouse.click = true;
	// get only clicks that happen on the canvas itself
	const canvas = document.querySelector('canvas');
	const rect = canvas.getBoundingClientRect();
	mouse.x = e.clientX - rect.left;
	mouse.y = e.clientY - rect.top;
	console.log(mouse.x, mouse.y);
}

function mouseUp(e) {
	mouse.click = false;
	mouse.x = null;
	mouse.y = null;
}



export function onInit() {
	const canvas = document.querySelector('canvas');
	canvas.addEventListener('mousedown', mouseDown);
	canvas.addEventListener('mouseup', mouseUp);
}