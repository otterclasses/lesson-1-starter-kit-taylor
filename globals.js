import { Spritesheet } from './spritesheet.js';
import { Position, Texture, CircleHitbox, Health, Mana } from './components';

// everything in our world (aside from the map)
export let Tokens = [];

// assets that we need
Spritesheet.image = new Image();
Spritesheet.image.src = "assets/spritesheet.png";
export let SlashImg = new Image();
SlashImg.src = "assets/sword-swipe_72x72.png";

// configuration variables
export const DEBUG = true;
export const TileSize = 48;
export const CameraWidth = 640;
export const CameraHeight = 480;

//sets the level sequence (go to levels to change)
export const Levels = [];

// global settings
export const Settings = {
  curLevel: 0,
  map: null,
  bombCooldown: 0,
  weaponCooldown: 0
}


export let Player = {
  position: new Position(0, 0),
  texture: new Texture('npc_paladin', 48, 60),
  hitbox: new CircleHitbox(18),
  health: new Health(10),
  mana: new Mana(10),
  bombs: 3,
  gold: 0,
  speed: 144,
  direction: 'left',
}

export let Weapon = {
  position: new Position(0,0),
  texture: new Texture('weapon_sword_golden', 48, 96),
  hitbox: new CircleHitbox(32, 24),
  damage: 2,
  knockback: 24,
  incorporeal: true,
}

export function resetPlayer() {
  Player.health.hp = Player.health.maxHp;
  Player.mana.mp = Player.mana.maxMp;
  Player.bombs = 3;
}