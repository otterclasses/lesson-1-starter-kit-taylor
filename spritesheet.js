export var Spritesheet = {
  "size": {
      "w": 512,
      "h": 256
  },
  "tileSize": 16,
  "tiles": [
      {
          "name": "wall_top_left",
          "bounds": {
              "x": 0,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_left",
          "bounds": {
              "x": 0,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_top_center",
          "bounds": {
              "x": 16,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_stain_1",
          "bounds": {
              "x": 0,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_center",
          "bounds": {
              "x": 16,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_top_right",
          "bounds": {
              "x": 32,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_stain_2",
          "bounds": {
              "x": 16,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_right",
          "bounds": {
              "x": 32,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_pipe_1",
          "bounds": {
              "x": 48,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "gargoyle_top_1",
          "bounds": {
              "x": 0,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "skull",
          "bounds": {
              "x": 16,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_stain_3",
          "bounds": {
              "x": 32,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_goo",
          "bounds": {
              "x": 48,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_pipe_2",
          "bounds": {
              "x": 64,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_red_1",
          "bounds": {
              "x": 0,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_plain",
          "bounds": {
              "x": 32,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_stain_goo",
          "bounds": {
              "x": 48,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_drain_gate",
          "bounds": {
              "x": 64,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_green_1",
          "bounds": {
              "x": 16,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_red_basin",
          "bounds": {
              "x": 0,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "stairs_top",
          "bounds": {
              "x": 80,
              "y": 0,
              "w": 32,
              "h": 16
          }
      },
      {
          "name": "wall_missing_brick_1",
          "bounds": {
              "x": 64,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "gargoyle_top_2",
          "bounds": {
              "x": 48,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_blue_1",
          "bounds": {
              "x": 32,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_green_basin",
          "bounds": {
              "x": 16,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "stairs_mid",
          "bounds": {
              "x": 80,
              "y": 16,
              "w": 32,
              "h": 16
          }
      },
      {
          "name": "floor_mud_nw",
          "bounds": {
              "x": 0,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_missing_brick_2",
          "bounds": {
              "x": 64,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_red_2",
          "bounds": {
              "x": 48,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_blue_basin",
          "bounds": {
              "x": 32,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "stairs_bottom",
          "bounds": {
              "x": 80,
              "y": 32,
              "w": 32,
              "h": 16
          }
      },
      {
          "name": "wall_gratings",
          "bounds": {
              "x": 112,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_n_1",
          "bounds": {
              "x": 16,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_w",
          "bounds": {
              "x": 0,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_pipe_small",
          "bounds": {
              "x": 80,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_green_2",
          "bounds": {
              "x": 64,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_red_puddle",
          "bounds": {
              "x": 48,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_edge_1",
          "bounds": {
              "x": 128,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_edge_2",
          "bounds": {
              "x": 112,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_n_2",
          "bounds": {
              "x": 32,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_mid_1",
          "bounds": {
              "x": 16,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_sw",
          "bounds": {
              "x": 0,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_wooden",
          "bounds": {
              "x": 144,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_small",
          "bounds": {
              "x": 112,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "column_wall",
          "bounds": {
              "x": 96,
              "y": 48,
              "w": 16,
              "h": 48
          }
      },
      {
          "name": "wall_gargoyle_blue_2",
          "bounds": {
              "x": 80,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_green_puddle",
          "bounds": {
              "x": 64,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_edge_3",
          "bounds": {
              "x": 128,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_ne",
          "bounds": {
              "x": 48,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_mid_2",
          "bounds": {
              "x": 32,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_s_1",
          "bounds": {
              "x": 16,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_zombie_small",
          "bounds": {
              "x": 0,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_rusty",
          "bounds": {
              "x": 160,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_chopper",
          "bounds": {
              "x": 128,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "floor_gargoyle_blue_puddle",
          "bounds": {
              "x": 80,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "black",
          "bounds": {
              "x": 64,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_e",
          "bounds": {
              "x": 48,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_s_2",
          "bounds": {
              "x": 32,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_skelet",
          "bounds": {
              "x": 16,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_goblin",
          "bounds": {
              "x": 0,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_steel",
          "bounds": {
              "x": 176,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_golden",
          "bounds": {
              "x": 144,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_black",
          "bounds": {
              "x": 112,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "box",
          "bounds": {
              "x": 80,
              "y": 96,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "floor_light",
          "bounds": {
              "x": 64,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_mud_se",
          "bounds": {
              "x": 48,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_zombie",
          "bounds": {
              "x": 32,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_orc_masked",
          "bounds": {
              "x": 16,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_imp",
          "bounds": {
              "x": 0,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_ruby",
          "bounds": {
              "x": 192,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_emerald",
          "bounds": {
              "x": 160,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_green",
          "bounds": {
              "x": 128,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "boxes_stacked",
          "bounds": {
              "x": 96,
              "y": 96,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "hero_basic",
          "bounds": {
              "x": 64,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_zombie_green",
          "bounds": {
              "x": 48,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_orc",
          "bounds": {
              "x": 32,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_wogol",
          "bounds": {
              "x": 16,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_earth",
          "bounds": {
              "x": 0,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_big",
          "bounds": {
              "x": 208,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_katana_silver",
          "bounds": {
              "x": 176,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_red",
          "bounds": {
              "x": 144,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "monster_bat",
          "bounds": {
              "x": 64,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_orc_armored",
          "bounds": {
              "x": 48,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_bies",
          "bounds": {
              "x": 32,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_water",
          "bounds": {
              "x": 16,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_earth_small",
          "bounds": {
              "x": 0,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "hand",
          "bounds": {
              "x": 81,
              "y": 129,
              "w": 4,
              "h": 4
          }
      },
      {
          "name": "hand_small",
          "bounds": {
              "x": 81,
              "y": 134,
              "w": 3,
              "h": 3
          }
      },
      {
          "name": "hand_big",
          "bounds": {
              "x": 81,
              "y": 138,
              "w": 5,
              "h": 5
          }
      },
      {
          "name": "weapon_dagger_long",
          "bounds": {
              "x": 112,
              "y": 109,
              "w": 7,
              "h": 19
          }
      },
      {
          "name": "waepon_katana",
          "bounds": {
              "x": 224,
              "y": 0,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_sword_silver",
          "bounds": {
              "x": 192,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_dagger_small",
          "bounds": {
              "x": 128,
              "y": 96,
              "w": 8,
              "h": 16
          }
      },
      {
          "name": "monster_necromancer",
          "bounds": {
              "x": 80,
              "y": 144,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_orc_veteran",
          "bounds": {
              "x": 64,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_chort",
          "bounds": {
              "x": 48,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_fire",
          "bounds": {
              "x": 32,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_water_small",
          "bounds": {
              "x": 16,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_gold_tall",
          "bounds": {
              "x": 0,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "gargoyle_mid_top_left",
          "bounds": {
              "x": 160,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_hammer",
          "bounds": {
              "x": 120,
              "y": 109,
              "w": 8,
              "h": 19
          }
      },
      {
          "name": "weapon_dagger_steel",
          "bounds": {
              "x": 136,
              "y": 96,
              "w": 8,
              "h": 16
          }
      },
      {
          "name": "monster_dark_knight",
          "bounds": {
              "x": 96,
              "y": 139,
              "w": 16,
              "h": 21
          }
      },
      {
          "name": "weapon_sledgehammer",
          "bounds": {
              "x": 240,
              "y": 0,
              "w": 16,
              "h": 48
          }
      },
      {
          "name": "weapon_silver_topaz",
          "bounds": {
              "x": 208,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "weapon_dagger_golden",
          "bounds": {
              "x": 144,
              "y": 96,
              "w": 8,
              "h": 16
          }
      },
      {
          "name": "darkness_top",
          "bounds": {
              "x": 128,
              "y": 112,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "monster_orc_shaman",
          "bounds": {
              "x": 80,
              "y": 160,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_rokita",
          "bounds": {
              "x": 64,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_air",
          "bounds": {
              "x": 48,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_fire_small",
          "bounds": {
              "x": 32,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_gold_short",
          "bounds": {
              "x": 16,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "gargoyle_mid_top_right",
          "bounds": {
              "x": 176,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_green_left",
          "bounds": {
              "x": 160,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "weapon_dagger_silver",
          "bounds": {
              "x": 152,
              "y": 96,
              "w": 8,
              "h": 16
          }
      },
      {
          "name": "weapon_sword_topaz",
          "bounds": {
              "x": 224,
              "y": 32,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "monster_demonolog",
          "bounds": {
              "x": 80,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_plant",
          "bounds": {
              "x": 64,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_air_small",
          "bounds": {
              "x": 48,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_zombie_tall",
          "bounds": {
              "x": 32,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_flag_red",
          "bounds": {
              "x": 192,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_green_right",
          "bounds": {
              "x": 176,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_green_left",
          "bounds": {
              "x": 160,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_nw",
          "bounds": {
              "x": 256,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "torch_no_flame",
          "bounds": {
              "x": 113,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "darkness_left",
          "bounds": {
              "x": 160,
              "y": 112,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "monster_ogre",
          "bounds": {
              "x": 96,
              "y": 176,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "monster_elemental_goo",
          "bounds": {
              "x": 80,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_plant_small",
          "bounds": {
              "x": 64,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_zombie_short",
          "bounds": {
              "x": 48,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_flag_green",
          "bounds": {
              "x": 208,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_green_right",
          "bounds": {
              "x": 176,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_blue_left",
          "bounds": {
              "x": 192,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_n",
          "bounds": {
              "x": 272,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_w",
          "bounds": {
              "x": 256,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "torch_1",
          "bounds": {
              "x": 129,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "weapon_bomb",
          "bounds": {
              "x": 240,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "monster_elemental_goo_small",
          "bounds": {
              "x": 80,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_flag_blue",
          "bounds": {
              "x": 224,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_blue_right",
          "bounds": {
              "x": 208,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_blue_left",
          "bounds": {
              "x": 192,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_ne",
          "bounds": {
              "x": 288,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_front",
          "bounds": {
              "x": 272,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_w2",
          "bounds": {
              "x": 256,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "torch_2",
          "bounds": {
              "x": 145,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "darkness_right",
          "bounds": {
              "x": 192,
              "y": 112,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "monster_demon",
          "bounds": {
              "x": 128,
              "y": 176,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "wall_flag_yellow",
          "bounds": {
              "x": 240,
              "y": 64,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_red_left",
          "bounds": {
              "x": 224,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_blue_right",
          "bounds": {
              "x": 208,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_nw",
          "bounds": {
              "x": 304,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_e",
          "bounds": {
              "x": 288,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_sw",
          "bounds": {
              "x": 256,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "torch_3",
          "bounds": {
              "x": 161,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "npc_sage",
          "bounds": {
              "x": 96,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "wall_gargoyle_mid_red_right",
          "bounds": {
              "x": 240,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_red_left",
          "bounds": {
              "x": 224,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_ne",
          "bounds": {
              "x": 320,
              "y": 0,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_e2",
          "bounds": {
              "x": 288,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_w",
          "bounds": {
              "x": 304,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outline_left",
          "bounds": {
              "x": 256,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "flask_red",
          "bounds": {
              "x": 116,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "torch_4",
          "bounds": {
              "x": 177,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "darkness_bottom",
          "bounds": {
              "x": 224,
              "y": 112,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "monster_tentackle",
          "bounds": {
              "x": 160,
              "y": 176,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "npc_trickster",
          "bounds": {
              "x": 112,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_merchant",
          "bounds": {
              "x": 96,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "floor_gargoyle_mid_red_right",
          "bounds": {
              "x": 240,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outer_se",
          "bounds": {
              "x": 288,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_e",
          "bounds": {
              "x": 320,
              "y": 16,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_sw",
          "bounds": {
              "x": 304,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outline_1",
          "bounds": {
              "x": 272,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "flask_green",
          "bounds": {
              "x": 132,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "torch_5",
          "bounds": {
              "x": 193,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "npc_mage",
          "bounds": {
              "x": 128,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_merchant_2",
          "bounds": {
              "x": 112,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_inner_se",
          "bounds": {
              "x": 320,
              "y": 32,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_front_left",
          "bounds": {
              "x": 304,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_outline_2",
          "bounds": {
              "x": 288,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "flask_blue",
          "bounds": {
              "x": 148,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "torch_6",
          "bounds": {
              "x": 209,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "npc_wizzard",
          "bounds": {
              "x": 144,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_dwarf",
          "bounds": {
              "x": 128,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Wall_front_right",
          "bounds": {
              "x": 320,
              "y": 48,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "door_left",
          "bounds": {
              "x": 352,
              "y": 16,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "Wall_outline_2",
          "bounds": {
              "x": 304,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "Floor_stain_nw",
          "bounds": {
              "x": 272,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "flask_big_red",
          "bounds": {
              "x": 196,
              "y": 180,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "flask_yellow",
          "bounds": {
              "x": 164,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "torch_7",
          "bounds": {
              "x": 225,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "column",
          "bounds": {
              "x": 208,
              "y": 176,
              "w": 16,
              "h": 48
          }
      },
      {
          "name": "npc_barbarian",
          "bounds": {
              "x": 160,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_dwarf_2",
          "bounds": {
              "x": 144,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "door_closed",
          "bounds": {
              "x": 368,
              "y": 16,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "Wall_outline_right",
          "bounds": {
              "x": 320,
              "y": 64,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "Floor_stain_n",
          "bounds": {
              "x": 288,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Floor_stain_w",
          "bounds": {
              "x": 272,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "flask_big_green",
          "bounds": {
              "x": 196,
              "y": 196,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "flask_big_yellow",
          "bounds": {
              "x": 180,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "torch_8",
          "bounds": {
              "x": 241,
              "y": 152,
              "w": 14,
              "h": 23
          }
      },
      {
          "name": "chest_closed",
          "bounds": {
              "x": 224,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_wrestler",
          "bounds": {
              "x": 176,
              "y": 224,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_elf",
          "bounds": {
              "x": 160,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Floor_stain_ne",
          "bounds": {
              "x": 304,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Floor_ladder",
          "bounds": {
              "x": 288,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "flask_big_blue",
          "bounds": {
              "x": 196,
              "y": 212,
              "w": 9,
              "h": 11
          }
      },
      {
          "name": "chest_open_empty",
          "bounds": {
              "x": 224,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "chest_golden_closed",
          "bounds": {
              "x": 240,
              "y": 176,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_elf_2",
          "bounds": {
              "x": 176,
              "y": 240,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "door_right",
          "bounds": {
              "x": 400,
              "y": 16,
              "w": 16,
              "h": 32
          }
      },
      {
          "name": "Floor_stain_e",
          "bounds": {
              "x": 304,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_knight_yellow",
          "bounds": {
              "x": 192,
              "y": 236,
              "w": 16,
              "h": 20
          }
      },
      {
          "name": "chest_open_full",
          "bounds": {
              "x": 224,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "chest_golden_open_empty",
          "bounds": {
              "x": 240,
              "y": 192,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Pit_nw",
          "bounds": {
              "x": 336,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_knight_green",
          "bounds": {
              "x": 208,
              "y": 236,
              "w": 16,
              "h": 20
          }
      },
      {
          "name": "chest_golden_open_full",
          "bounds": {
              "x": 240,
              "y": 208,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "door_open",
          "bounds": {
              "x": 432,
              "y": 16,
              "w": 32,
              "h": 32
          }
      },
      {
          "name": "Pit_n",
          "bounds": {
              "x": 352,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Pit_w",
          "bounds": {
              "x": 336,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_knight_blue",
          "bounds": {
              "x": 224,
              "y": 236,
              "w": 16,
              "h": 20
          }
      },
      {
          "name": "Pit_ne",
          "bounds": {
              "x": 368,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Pit_sw",
          "bounds": {
              "x": 336,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "npc_paladin",
          "bounds": {
              "x": 240,
              "y": 236,
              "w": 16,
              "h": 20
          }
      },
      {
          "name": "Pit_e",
          "bounds": {
              "x": 368,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Pit_s",
          "bounds": {
              "x": 352,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_nw",
          "bounds": {
              "x": 400,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Pit_se",
          "bounds": {
              "x": 368,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_n",
          "bounds": {
              "x": 416,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_w",
          "bounds": {
              "x": 400,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_ne",
          "bounds": {
              "x": 432,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_sw",
          "bounds": {
              "x": 400,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_bricks",
          "bounds": {
              "x": 400,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_e",
          "bounds": {
              "x": 432,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_s",
          "bounds": {
              "x": 416,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_single_crack",
          "bounds": {
              "x": 448,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_se",
          "bounds": {
              "x": 432,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_single",
          "bounds": {
              "x": 448,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_nwe",
          "bounds": {
              "x": 464,
              "y": 80,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_swe",
          "bounds": {
              "x": 464,
              "y": 96,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_nsw",
          "bounds": {
              "x": 448,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_ns",
          "bounds": {
              "x": 432,
              "y": 128,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_nse",
          "bounds": {
              "x": 464,
              "y": 112,
              "w": 16,
              "h": 16
          }
      },
      {
          "name": "Edge_we",
          "bounds": {
              "x": 448,
              "y": 128,
              "w": 16,
              "h": 16
          }
      }
  ]
}
Spritesheet.tilesById = {};
for (let slice of Spritesheet.tiles) {
  Spritesheet.tilesById[slice.name] = slice;
}

/**
 * 
 * @param {*} name 
 * @returns { x:number, y:number, w:number, h:number } location of tile
 */
export function getTileById(name) {
    if (!(name in Spritesheet.tilesById))
        throw new Error(`Spritesheet does not contain '${name}' id for tileById lookup`);
    return Spritesheet.tilesById[name].bounds;
}

/**
 * Fetches the x, y, width, and height of tiled index from spritesheet location
 * @param {number} index index from Tiled maps
 * @returns { x:number, y:number, w:number, h:number } location of tile
 */
export function getTiledTile(index) {
    // index is based on tile sized grid on width/height
    const width = Spritesheet.size.w / Spritesheet.tileSize;
    const x = (index - 1) % width; // x + y where y is width
    const y = (index - x - 1) / width;
    return {
        x: x * Spritesheet.tileSize,
        y: y * Spritesheet.tileSize,
        w: Spritesheet.tileSize,
        h: Spritesheet.tileSize
    }
}

const _indexCache = {};
export function getTiledId(index) {
    if (index in _indexCache)
        return _indexCache[index];
        
    const tile = getTiledTile(index);
    let match = Spritesheet.tiles.find(slice => slice.bounds.x == tile.x && slice.bounds.y == tile.y);
    if (match == undefined)
        return undefined;
    // otherwise store this name
    _indexCache[index] = match;
    return match;
}