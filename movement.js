import { isCollision } from "./collision";
import { getMapTile } from "./levels";
import { Walkable } from "./levels";
import { clamp, normalize } from './utils.js';
import { updateHealth } from "./combat";
import { Tokens, Settings, TileSize, Player } from "./globals";

const InteractTiles = {
    161 : () => { console.log("RED BASIN!") },
    162 : () => { console.log("GREEN BASIN!") },
    163 : () => { console.log("BLUE BASIN!") }
}

/**
 * Checks to see if the tile is walkable or not.
 * @param {*} x 
 * @param {*} y 
 * @returns true if thhe tile is walkable, false if it is not.
 */
export function isWalkable (x, y) {
    let tileX = Math.floor(x / TileSize);
    let tileY = Math.floor(y / TileSize);
    let tileId = getMapTile(Settings.map, tileX, tileY);
    for(let x = 0; x < Walkable.length; x++) {
        if (tileId == Walkable[x]) {
            return true;
        }
    };
    return false;
}

export function isInteractable(x, y) {
    let tileX = Math.floor(x / TileSize);
    let tileY = Math.floor(y / TileSize);
    let tileId = getMapTile(Settings.map, tileX, tileY);
    if (tileId in InteractTiles) {
        let callback = InteractTiles[tileId]
        callback();
    }
}

export function movementCheck(mover, x, y) {
    for (let i = 0; i < Tokens.length; i++) {
        if (Tokens[i] == mover || !('hitbox' in Tokens[i]) || Tokens[i].hitbox.sensor || 'incorporeal' in Tokens[i]) {
            continue;
        }
        if (isCollision(x, y, mover.hitbox, Tokens[i].position.x, Tokens[i].position.y, Tokens[i].hitbox)) {
            return false;
        }
    }
    return true;
}

export function mobPathing(dt) {
    for (let i = 0; i < Tokens.length; i++) {
        if (!('behavior' in Tokens[i])) {
            continue;
        }
        let dx = Player.position.x - Tokens[i].position.x;
        let dy = Player.position.y - Tokens[i].position.y;
        let distance = Math.sqrt(dx*dx + dy*dy);
        if (distance <= Tokens[i].behavior.aggroRadius) {
            Tokens[i].behavior.aggro = true;
        } else if(distance >= Tokens[i].behavior.deaggroRadius && Tokens[i].behavior.aggroCooldown <= 0) {
            Tokens[i].behavior.aggro = false;
        }
        if (Tokens[i].behavior.aggro) {
            let vector = normalize(dx, dy);
            let nextX = Tokens[i].position.x + vector.x * Tokens[i].speed * dt;
            let nextY = Tokens[i].position.y + vector.y * Tokens[i].speed * dt;

            /** Faces the mob in the direction they're trying to move */
            if (nextX > Tokens[i].position.x) {
                Tokens[i].texture.faceRight = true;
            } else if (nextX < Tokens[i].position.x) {
                Tokens[i].texture.faceRight = false;
            }

            /**If the mob would collide with the player, it attacks instead of moving */
            if (isCollision(nextX, Tokens[i].position.y, Tokens[i].hitbox, Player.position.x, Player.position.y, Player.hitbox) ||
                isCollision(Tokens[i].position.x, nextY, Tokens[i].hitbox, Player.position.x, Player.position.y, Player.hitbox)) {
                mobAttack(Tokens[i]);
                continue;
                }

            /**Allows the ghost to ignore obstacles and terrain. */
            if ("incorporeal" in Tokens[i]) {
                Tokens[i].position.x += vector.x * Tokens[i].speed * dt;
                Tokens[i].position.y += vector.y * Tokens[i].speed * dt;
                continue;
            }
            
            /**Moves everyone else */
            if (movementCheck(Tokens[i], nextX, Tokens[i].position.y) && 
                isWalkable(nextX, Tokens[i].position.y + Tokens[i].texture.h/2)) {
                Tokens[i].position.x = nextX;
            }
            if (movementCheck(Tokens[i], Tokens[i].position.x, nextY) && 
                isWalkable(Tokens[i].position.x, nextY + Tokens[i].texture.h/2)) {
                Tokens[i].position.y = nextY;
            }
        }
    }
}

function mobAttack(mob) {
    if (mob.cooldown <= 0) {
        console.log("player suffers", mob.damage, " damage from a", mob.type);
        updateHealth(Player, -mob.damage);
        mob.cooldown = 1;
    }
}

export function projectilePathing(dt) {
    for (let i = Tokens.length - 1; i >= 0; i--) {
        //skips over tokens that aren't projectiles
        if (!('type' in Tokens[i]) || Tokens[i].type != 'projectile') {
            continue;
        }
        let nextX = Tokens[i].position.x;
        let nextY = Tokens[i].position.y;
        let direction = Tokens[i].direction;
        if (direction == 'up') {
            nextY = Tokens[i].position.y - Tokens[i].speed * dt;
        } else if (direction == 'down') {
            nextY = Tokens[i].position.y + Tokens[i].speed * dt;
        } else if (direction == 'right') {
            nextX = Tokens[i].position.x + Tokens[i].speed * dt;
        } else {
            nextX = Tokens[i].position.x - Tokens[i].speed * dt;
        }

        if (isCollision(Player.position.x, Player.position.y, Player.hitbox, nextX, nextY, Tokens[i].hitbox)) {
            updateHealth(Player, -Tokens[i].damage);
            Tokens.splice(Tokens.indexOf(Tokens[i]), 1);
            continue;
        }

        Tokens[i].position.x = nextX;
        Tokens[i].position.y = nextY;
    }
}