export const CIRCLE = "circle";
export const RECT = "rectangle";

function distance(x1, y1, x2, y2) {
    // remember middle school?
    const dx = x2 - x1;
    const dy = y2 - y1;
    return Math.sqrt(dx * dx + dy * dy);
}

/**
 * Checks for collision
 * @param {number} x 
 * @param {number} y 
 * @param {number} w 
 * @param {number} h 
 * @param {object} b object to check collision against
 * @returns 
 */
export function isCollision(x1, y1, hitbox1, x2, y2, hitbox2) {
    // if ((player.x == (box.x + box.w)) && ((player.y <= box.y) && (player.y >= (box.y + h)))) {
    //     player.x = box.x + box.w + 48;
    // }
    //let a = player;
    if (hitbox1.type == CIRCLE && hitbox2.type == CIRCLE) {
        return circleCollision(x1, y1, hitbox1, x2, y2, hitbox2);
    } else if (hitbox1.type == RECT && hitbox2.type == RECT) {
        return rectCollision(x1, y1, hitbox1, x2, y2, hitbox2);
    } else if (hitbox1.type == CIRCLE) {
        return mixedCollision(x1, y1, hitbox1, x2, y2, hitbox2);
    } else {
        return mixedCollision(x2, y2, hitbox2, x1, y1, hitbox1);
    }
    

}

function circleCollision(x1, y1, hitbox1, x2, y2, hitbox2) {
    let ox1 = x1 + (hitbox1.offsetX || 0);
    let oy1 = y1 + (hitbox1.offsetY || 0);
    let ox2 = x2 + (hitbox2.offsetX || 0);
    let oy2 = y2 + (hitbox2.offsetY || 0);
    return (distance(ox1, oy1, ox2, oy2) <= (hitbox1.r + hitbox2.r));
}

function rectCollision(x1, y1, hitbox1, x2, y2, hitbox2) {
    let ox1 = x1 - hitbox1.w / 2 + (hitbox1.offsetX || 0);
    let oy1 = y1 - hitbox1.h / 2 + (hitbox1.offsetY || 0);
    let ox2 = x2 - hitbox2.w / 2 + (hitbox2.offsetX || 0);
    let oy2 = y2 - hitbox2.h / 2 + (hitbox2.offsetY || 0);

    return (ox1 + hitbox1.w) >= (ox2) &&
        (ox1) <= (ox2 + hitbox2.w) &&
        (oy1 + hitbox1.h) >= (oy2) &&
        (oy1) <= (oy2 + hitbox2.h);

}

function mixedCollision(circlex, circley, hitbox1, rectx, recty, hitbox2) {
    // calculate offsets
    const cx = circlex + (hitbox1.offsetX || 0);
    const cy = circley + (hitbox1.offsetY || 0);
    const rx = rectx - hitbox2.w / 2 + (hitbox2.offsetX || 0);
    const ry = recty - hitbox2.h / 2 + (hitbox2.offsetY || 0);

    let testX = cx;
    let testY = cy;
  
    if (cx < rx) {
        testX = rx;
    } else if (cx > rx + hitbox2.w) {
        testX = rx + hitbox2.w;
    }
  
    if (cy < ry) {
        testY = ry;
    } else if (cy > ry + hitbox2.h) {
        testY = ry + hitbox2.h;
    }
  
    let dist = distance(cx, cy, testX, testY);
  
    return dist < hitbox1.r;
}

export function sensorCheck(collider, tokens) {
    /**For Each loop. Simply superior... */
    // for (let token of tokens) {

    // }

    for (let i = tokens.length - 1; i >= 0; i--) {
        if (!('hitbox' in tokens[i]) || !tokens[i].hitbox.sensor || tokens[i].hitbox.onCollision == undefined) {
            continue;
        }
        if (isCollision(collider.position.x, collider.position.y, collider.hitbox, tokens[i].position.x, tokens[i].position.y, tokens[i].hitbox)) {
            tokens[i].hitbox.onCollision(tokens[i], collider);
        }
    }
}