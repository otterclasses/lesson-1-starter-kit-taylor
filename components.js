import { CIRCLE, RECT } from './collision.js';

export class Position {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

export class Texture {
    constructor(imageId, width, height, customImg = undefined) {
        this.imageId = imageId;
        this.w = width;
        this.h = height;
        this.customImg = customImg;
        this.faceRight = false;
    }
}

export class Hitbox {
    constructor(type, offsetY = undefined) {
        this.type = type;
        this.offsetY = offsetY;
        this.sensor = false;
    }

    addSensor(onCollision = undefined) {
        this.sensor = true;
        this.onCollision = onCollision;
    }
}

export class RectHitbox extends Hitbox {
    constructor(w, h, offsetY = undefined) {
        super(RECT, offsetY);
        this.w = w;
        this.h = h;
    }
}

export class CircleHitbox extends Hitbox {
    constructor(r, offsetY = undefined) {
        super(CIRCLE, offsetY);
        this.r = r;
    }
}

export class Health {
    constructor(maxHp) {
        this.maxHp = maxHp;
        this.hp = maxHp;
    }
}

export class Mana {
    constructor(maxMp) {
        this.maxMp = maxMp;
        this.mp = maxMp;
    }
}

export class Loot {
    constructor(red, blue, green, yellow) {
        this.lootTable = [];
        for (let i = 0; i < red; i++) {
            this.lootTable.push('flask_red');
        }
        for (let i = 0; i < blue; i++) {
            this.lootTable.push('flask_blue');
        }
        for (let i = 0; i < green; i++) {
            this.lootTable.push('flask_green');
        }
        for (let i = 0; i < yellow; i++) {
            this.lootTable.push('flask_yellow');
        }
    }
}

export class Behavior {
    constructor(aggroRadius, deaggroRadius) {
        this.aggroRadius = aggroRadius;
        this.deaggroRadius = deaggroRadius;
        this.aggro = false;
        this.aggroCooldown = 0;
    }
}

export class Ranged {
    constructor(projectile, distance) {
        this.projectile = projectile;
        this.distance = distance;
    }
}