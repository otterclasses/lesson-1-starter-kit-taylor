import { Tokens, Player, Weapon } from "./globals";
import { isCollision } from "./collision";
import { createCorpse, createLoot, createBonfire, createHammer, createDagger } from "./tokens";
//import { restartGame } from "./levels";

export function attack(attacker, weapon) {
    for (let i = Tokens.length - 1; i >= 0; i--) {
        //skips tokens w/o health or a hitbox as well as the player
        if (!('health' in Tokens[i]) || !('hitbox' in Tokens[i]) || Tokens[i] == attacker || Tokens[i] == weapon) {
            continue;
        }
        // we know it's in here
        const { position: enemyPos, hitbox: enemyHitbox } = Tokens[i];
        if (isCollision(weapon.position.x, weapon.position.y, weapon.hitbox, enemyPos.x, enemyPos.y, enemyHitbox)) {
            console.log("found collision", weapon, Tokens[i]);

            /**Senda the enemy away from the player if the weapon as knockback */
            if ('knockback' in weapon) {
                // if (Player.position.x < Tokens[i].position.x) {
                //     Tokens[i].position.x += weapon.knockback;
                // } else {
                //     Tokens[i].position.x -= weapon.knockback;
                // }
                
                if (Player.direction == 'up') {
                    Tokens[i].position.y -= weapon.knockback;
                } else if (Player.direction == 'down') {
                    Tokens[i].position.y += weapon.knockback;
                } else if (Player.direction == 'right') {
                    Tokens[i].position.x += weapon.knockback;
                } else {
                    Tokens[i].position.x -= weapon.knockback;
                }
            }
            updateHealth(Tokens[i], -weapon.damage);
        }
    }
}

export function updateHealth(token, change) {
    token.health.hp += change;
    if (token.health.hp > token.health.maxHp) {
        token.health.hp = token.health.maxHp
    }
    if (token.health.hp <= 0) {
        token.health.hp = 0;
        let corpse = createCorpse(token.position.x, token.position.y);
        Tokens.push(corpse);
        if ('loot' in token) {
            lootDrop(token.position.x, token.position.y, token.loot.lootTable);
        }
        Tokens.splice(Tokens.indexOf(token), 1);
        //resets the game if the Player hp reaches 0
        // if (Player.health.hp <= 0) {
        //     restartGame();
        // }
    
    //makes mobs aggressive for a while if you deal damage to them
    } else if ('behavior' in token) {
        token.behavior.aggro = true;
        token.behavior.aggroCooldown = 3;
    }
}

function lootDrop(x, y, lootTable) {
    if (lootTable.length > 0) {
        let odds = lootTable.length
        let roll = Math.floor(Math.random() * odds);
        let loot = createLoot(x, y, lootTable[roll]);
        Tokens.push(loot);
        console.log('loot drop:', lootTable[roll]);
    } else {
        console.log('No loot in lootTable');
    }
}

export function firebolt(dt) {
    for (let i = Tokens.length - 1; i >= 0; i--) {
        //skips over tokens that aren't firebolts
        if (!('type' in Tokens[i]) || Tokens[i].type != 'firebolt') {
            continue;
        }

        //determines the direction the firebolt moves in
        // let nextX = Tokens[i].position.x
        // if (Tokens[i].texture.faceRight) {
        //     nextX = Tokens[i].position.x + Tokens[i].speed * dt;
        // } else {
        //     nextX = Tokens[i].position.x - Tokens[i].speed * dt;
        // }

        let nextX = Tokens[i].position.x;
        let nextY = Tokens[i].position.y;
        let direction = Tokens[i].direction;
        if (direction == 'up') {
            nextY = Tokens[i].position.y - Tokens[i].speed * dt;
        } else if (direction == 'down') {
            nextY = Tokens[i].position.y + Tokens[i].speed * dt;
        } else if (direction == 'right') {
            nextX = Tokens[i].position.x + Tokens[i].speed * dt;
        } else {
            nextX = Tokens[i].position.x - Tokens[i].speed * dt;
        }

        // if (movementCheck(tokens[i], nextX, tokens[i].position.y)) {
        //     tokens[i].position.x = nextX;
        // }

        Tokens[i].position.x = nextX;
        Tokens[i].position.y = nextY;

        if (fireboltCollision(Tokens[i], Tokens[i].position.x)) {
            attack(Player, Tokens[i]);
            let bonfire = createBonfire(Tokens[i].position.x, Tokens[i].position.y);
            Tokens.splice(Tokens.indexOf(Tokens[i]), 1);
            Tokens.push(bonfire);
        }

    }
}

function fireboltCollision(firebolt, nextX) {
    for (let i = 0; i < Tokens.length; i++) {
        //skips over the player and other firebolts
        if (Tokens[i] == Player || Tokens[i] == Weapon || !('hitbox' in Tokens[i]) || 
            ('type' in Tokens[i] && (Tokens[i].type == 'firebolt' || Tokens[i].type == 'loot' || Tokens[i].type == 'bonfire'))) {
            continue;
        }

        if (isCollision(nextX, firebolt.position.y, firebolt.hitbox, Tokens[i].position.x, Tokens[i].position.y, Tokens[i].hitbox)) {
            console.log('firebolt hit something');
            return true;
        }
    }
    return false;
}

export function bonfire() {
    for (let i = 0; i < Tokens.length; i++) {
        if ('type' in Tokens[i] && Tokens[i].type == 'bonfire' && Tokens[i].cooldown <= 0) {
            console.log('bonfire burns');
            attack(Tokens[i], Tokens[i]);
            Tokens[i].cooldown = .5;
        }
    }
}

export function mobProjectiles() {
    for (let i = 0; i < Tokens.length; i++) {
        if (!('behavior' in Tokens[i] && 'ranged' in Tokens[i])) {
            continue;
        }

        let dx = Player.position.x - Tokens[i].position.x;
        let dy = Player.position.y - Tokens[i].position.y;
        let distance = Math.sqrt(dx*dx + dy*dy);

        let direction = playerDirection(Tokens[i]);
        //console.log(direction);

        if (Tokens[i].behavior.aggro && distance > Tokens[i].ranged.distance &&
        Tokens[i].cooldown <= 0 && direction != 'no shot') {
            console.log('shot fired');
            //let projectile = createHammer(Tokens[i].position.x, Tokens[i].position.y, direction);
            let projectile = getProjectile(Tokens[i], direction);
            Tokens.push(projectile);
            Tokens[i].cooldown = 1;
        }
    }
}

function getProjectile(token, direction) {
    console.log(token.ranged.projectile)
    if (token.ranged.projectile == 'hammer') {
        return createHammer(token.position.x, token.position.y, direction)
    }
    else if (token.ranged.projectile == 'dagger') {
        return createDagger(token.position.x, token.position.y, direction)
    }
}

function playerDirection(mob) {
    let playerX = Player.position.x;
    let playerY = Player.position.y;
    if (Math.abs(Player.position.x - mob.position.x) < 16) {
        console.log('same x');
        if (Player.position.y < mob.position.y) {
            return 'up';
        } else {
            return 'down';
        }
    } else if (Math.abs(Player.position.y - mob.position.y) < 16) {
        console.log('same y');
        if (Player.position.x < mob.position.x) {
            return 'left';
        } else {
            return 'right';
        }
    }
    return ('no shot');
}