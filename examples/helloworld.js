var myName = "Jamie";
var num1 = 13;
var num2 = 9.5;

function sayHello(someName, someNumber) {
  console.log("hello " + someName + ", my favorite # is " + someNumber);
}

// sayHello("Taylor", 13);
// sayHello("Jamie", 17);

function add(num1, num2) {
  return num1 + num2;
}

// var sum = add(15, 16);

// console.log(sum);

// for(var x = 0; x < 6; x++) {

// }


var names = ["Jamie", "Taylor", "Caroline", "Anne", "Kenny", "Poppy", "Bruno"];
var otherNames = ["donkey", "edward", "noah", "other-people"];
var noNames = [];

// console.log(names[2]);

// for(var x = 0; x < names.length; x++) {
//   // generates a random item in the list
//   var randy = Math.round(Math.random() * names.length);
//   console.log(names[randy]);
// }


function printRandNames(list, number) {

  if (list.length == 0) {
    console.log("you suck");
    return;
  }

  for (var x = 0; x < number; x++) {
    var randy = Math.round(Math.random() * (list.length - 1));
    console.log(list[randy]);
  }
}

// printRandNames(names, 3);
// printRandNames(otherNames, 1);
// printRandNames(noNames, 2);

function sayHello2(person) {
  console.log("hello " + person.name + ", my favorite # is " + person.favoriteNumber);
}

var kelos = {
  name: "Kelos",
  spec: "paladin",
  gender: "m",
  favoriteNumber: 69,
  stats: [3, 0, 2, 0, -1, 4]
}

var kai = {
  name: "Kai",
  spec: "bard",
  gender: "m",
  favoriteNumber: 420,
  stats: [1, 1, 1, 1, 1, 1]
}

function initiative(character) {
  console.log(character.stats[1] + Math.round(Math.random() * 19) + 1);
}

initiative(kelos);
initiative(kai);


class Character {
  constructor(name, spec, stats) {
    this.name = name;
    this.spec = spec;
    this.stats = stats;
  }

  sayHello() {
    console.log("Hello " + this.name);
  }

  initiative() {
    console.log(this.stats[1] + Math.round(Math.random() * 19) + 1);
  }
}

var annabelle = new Character("annabelle", "ranger", [0, 5, 0, 1, 2, 3]);
annabelle.sayHello();
annabelle.initiative();



function add(x, y) {
  return x + y;
}

var sum = add(123, 283);

// and and or
var num = 9;
// or
var yay = num > 9 || num > 3;
// or
var myName = 'kenny' == 'donkey' || num == 9
// and
myName = 'kenny' == 'kenny' && num == 9


// recursion is calling multiple functions
var tree = {
  node: {
    otherThing: {
      deeperYes: {
        dead: true
      }
    },
    noMeInstead: {
      dead: false
    }
  }
}
function traverse(tree) {
  // TODO
  traverse(tree);
}

traverse(tree);


// var, const, let
function confusingScope() {

  console.log(j); // 0

  for (var i = 0; i < 10; i++) {
    console.log("print 10 times");
    for (var j = 0; j < 10; j++) {
      console.log("print 100 times");
    }
  }
}

console.log(j); // undefined/error

// SUCKS bad VAR
function confusingScope() {
  var i = 0;
  var j = 0;

  for (i = 0; i < 10; i++) {
    console.log("print 10 times");
    for (j = 0; j < 10; j++) {
      console.log("print 100 times");
    }
  }
}

// let is better
function confusingScope() {
  console.log(j) // error

  for (let i = 0; i < 10; i++) {
    console.log("print 10 times");
    for (let j = 0; j < 10; j++) {
      console.log("print 100 times");
    }
  }
}

// const
const GRAVITY = 9.8;
const myFavNumber = 5;
// errors: myFavNumber = 13;

// objects though...
const myFavObject = {
  name: "Taylor"
}
// this is fine:
myFavObject.name = "kenny";
// error: myFavObject = {};

function fizzBuzz () {
  for (let x = 0; x <= 100; x++) {
    if (x % 3 == 0) {
      console.log("fizz");
    }
    if (x % 5 == 0) {
      console.log("buzz");
    }
    if ((x % 3 != 0) && (x % 5 != 0)) {
      console.log(x);
    }
  }
}