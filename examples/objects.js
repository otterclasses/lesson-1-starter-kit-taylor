// object oriented programming

function createBomb(x, y) {
  return {
    x: x,
    y: y,
    w: 48,
    h: 48,
    imageId: 'weapon_bomb',
    duration: 3,
    cooldown: 3,
    // how would we do this?
    countdown: (dt) => {
      this.duration -= dt;
    }
  }
}

class Bomb {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.w = 48;
    this.h = 48;
    this.imageId = 'weapon_bomb';
    this.duration = 3;
    this.cooldown = 3;
  }

  countdown(dt) {
    this.duration -= dt;
  }

  explode() {
    console.log('it went boom in the night');
    return 6;
  }
}

class WebBomb extends Bomb {
  addWeb() {
    console.log("WEB TIME!");
  }

  explode() {
    this.addWeb();
    return super.explode();
  }
}

const myNewBomb = new Bomb(13, 14);
myNewBomb.countdown(1);

const myOtherBomb = new Bomb(13, 15);
myOtherBomb.countdown(2);

const specialBomb = new WebBomb(14, 14);
specialBomb.countdown(3);
specialBomb.explode();

function makeThingExplode(entities, explodable) {
  let damage = explodable.explode();
  for (let entity of entities) {
    entity.health -= damage;
  }
}

// var
let startTime = {
  day: 31,
  month: 12,
  year: 2020,
  hour: 13,
  minute: 30
}
const monthsToDays = [31, 28, 31, 31, 30, 30, 31, 31, 30, 31, 30, 31];
// what is tomorrow?
function tomorrow(dateTime) {
  dateTime.day += 1;
  let numDays = monthsToDays[dateTime.month - 1];
  if (dateTime.day > numDays) {
    dateTime.month += 1;
    dateTime.day = 1;
    if (dateTime.month > 12) {
      dateTime.month = 1;
      dateTime.year += 1;
    }
  }
}

// let's try it out
console.log(startTime);
tomorrow(startTime);
console.log(startTime);

// end time
let endTime = {
  day: 2,
  month: 1,
  year: 2021,
  hour: 14,
  minute: 15
}

function getDiff(startTime, endTime) {
  const diff = {
    day: 0, month: 0, year: 0, hour: 0, minute: 0
  }
  diff.year = endTime.year - startTime.year;
  // oh god no this is a mistake
}

// let's do some dates
let now = new Date();


// why OOP is not always a good idea
class Entity {
  constructor(x, y, w, h, imageId) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.imageId = imageId;
  }
}

class Hitbox {
  constructor(offsetY=undefined) {
    this.offsetY = offsetY;
  }
}

class RectHitbox extends Hitbox {
  constructor(w, h, offsetY=undefined) {
    super(offsetY);
    this.w = w;
    this.h = h;
  }
}

class CircleHitbox extends Hitbox {
  constructor(r, offsetY=undefined) {
    super(offsetY);
    this.r = r;
  }
}

const myHitBox = new RectHitbox(12, 14);
const myOtherHitbox = new CircleHitbox(13);

if (myHitBox instanceof RectHitbox) {

} else if (myHitBox instanceof myOtherHitbox) {

}

class EntityCollidable extends Entity {
  constructor(x, y, w, h, imageId, hitbox) {
    super(x, y, w, h, imageId);
    this.hitbox = hitbox;
  }
}

class Bomb2 extends Entity {
  constructor(x, y, duration) {
    super(x, y, 48, 48, "weapon_bomb");
    this.duration = duration;
  }
}

// BAD ^^^^^^^

// OOP should be *composable*, think about what *composes* an object
class Position {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Texture {
  constructor(imageId, width, height) {
    this.imageId = imageId;
    this.width = width;
    this.height = height;
  }
}

let paladin = {
  x: 0,
  y: 0,
  texture: new Texture("paladin", 48, 48),
  hitbox: new CircleHitbox(24)
}




/* let player = {
  x: 48,
  y: 104,
  w: 48,
  h: 60,
  speed: 2,
  imageId: 'npc_paladin',
  hitbox: {
      type: CIRCLE,
      r: 18
  },
  faceRight: true
};

//I tried using 'h' and 'weapon.h' instead of 96, but it black screened.
let weapon = {
  x: 0,
  y: 0,
  w: 48,
  h: 96,
  imageId: 'weapon_sword_golden',
  damage: 1,
}

let zombie = {
  x: 240,
  y: 360,
  w: 48,
  h: 48,
  imageId: 'monster_zombie',
  hp: 10,
}

let skeleton = {
  x: 120,
  y: 360,
  w: 48,
  h: 48,
  imageId: 'monster_skelet',
}

let ghost = {
  x: 120,
  y: 360,
  w: 48,
  h: 48,
  imageId: 'monster_elemental_air',
}

let box = {
  x: 384,
  y: 240 + 24 * 2,
  w: 48,
  h: 96,
  imageId: 'box',
  hitbox: {
      type: RECT,
      offsetY: 20,
      w: 48,
      h: 48,
  }
} */

// we can extract properties from objects into local variables
const imageId = player.texture.imageId;
// local variable imagerrr now contains player.texture.imageId
const { imageId: imagerrr } = player.texture;

let zombie = {
  position: new Position(240, 360),
  texture: new Texture('monster_zombie', 48, 48),
  hitbox: new CircleHitbox(18),
  health: new Health(10),
  loot: new Loot(2, 0, 0, 1),
  behavior: new Behavior(96, 97),
  speed: 48,
  damage: 3,
  cooldown: 0,
}

let skeleton = {
  position: new Position(180, 360),
  texture: new Texture('monster_skelet', 48, 48),
  hitbox: new CircleHitbox(18),
  health: new Health(5),
  loot: new Loot(1, 0, 1, 1),
  behavior: new Behavior(96, 97),
  speed: 96,
  damage: 2,
  cooldown: 0,
}

let ghost = {
  position: new Position(120, 360),
  texture: new Texture('monster_elemental_air', 48, 48),
  hitbox: new CircleHitbox(18),
  health: new Health(8),
  loot: new Loot(1, 1, 0, 0),
  behavior: new Behavior(144, 145),
  speed: 96,
  damage: 1,
  cooldown: 0,
}

let box = {
  position: new Position(384, 240 + 24 * 2),
  texture: new Texture('box', 48, 96),
  hitbox: new RectHitbox(48, 48, 20),
}