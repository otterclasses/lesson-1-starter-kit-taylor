let tokens = ["Kenny"]

tokens.push("Taylor");
// [Kenny, Taylor]
tokens.splice(1, 1);
// [Kenny]
console.log(tokens);

let abc = ["c", "b", "a", "d"]
abc.sort();
console.log(abc);
// a, b, c, d

let players = [
  { name: "Kenny", score: 5 },
  { name: "Taylor", score: 8 },
  { name: "Edward", score: -1 }
]

function customSort(playerA, playerB) {
  if (playerA.score > playerB.score) {
    return -1
  } else if (playerA.score < playerB.score) {
    return 1;
  }
  return 0;
}
players.sort(customSort);
console.log(players);