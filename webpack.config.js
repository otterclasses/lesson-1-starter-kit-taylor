const path = require('path');

module.exports = {
  entry: './main.js',
  devServer: {
    static: './dist',
    port: 3000
  },
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    library: 'game'
  },
};