import { getTileById, getTiledTile, Spritesheet } from './spritesheet.js';
import { CIRCLE, RECT } from './collision.js';
import { TileSize, CameraHeight, CameraWidth , Player, Settings} from './globals.js';
import { getMapTile, getMapWidth, getMapHeight } from './levels.js';

export function drawCircle(x, y, radius) {
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.strokeStyle = '#ff2bf1';
    ctx.stroke();
}

export function drawRectangle(x, y, w, h) {
    ctx.rect(x, y, w, h);
    ctx.strokeStyle = '#ff2bf1';
    ctx.stroke();
}

export function drawToken(token, flip=false) {
    let x = token.position.x - token.texture.w / 2;
    let y = token.position.y - token.texture.h / 2;
    let width = token.texture.w;
    if (flip) {
        ctx.save();
        ctx.scale(-1, 1);
        x = -x;
        width = -width;
    }

    if (token.texture.customImg == undefined) {
        let img = getTileById(token.texture.imageId);
        ctx.drawImage(Spritesheet.image, img.x, img.y, img.w, img.h, x, y, width, token.texture.h);
    } else {
        ctx.drawImage(token.texture.customImg, x, y, width, token.texture.h);
    }

    if (flip) {
        ctx.restore();
    }
}

export function drawDebug(token) {
    drawCircle(token.position.x, token.position.y, 4); //Center of the token
    if (!token.hitbox)
        return;

    if (token.hitbox.type == RECT) {
        let ox = token.position.x - token.hitbox.w / 2 + (token.hitbox.offsetX || 0);
        let oy = token.position.y - token.hitbox.h / 2 + (token.hitbox.offsetY || 0);
        drawRectangle(ox, oy, token.hitbox.w, token.hitbox.h);
    } else if (token.hitbox.type == CIRCLE) {
        let ox = token.position.x + (token.hitbox.offsetX || 0);
        let oy = token.position.y + (token.hitbox.offsetY || 0);
        drawCircle(ox, oy, token.hitbox.r);
    }
}

function drawTile(x, y, index) {
    let tile = getTiledTile(index);
    ctx.drawImage(Spritesheet.image, tile.x, tile.y, tile.w, tile.h, x * TileSize, y * TileSize, TileSize, TileSize);
}

export function drawMap(map) {
    for (let y = 0; y < map.height; y++) {
        for (let x = 0; x < map.width; x++) {
            let tile = getMapTile(map, x, y);
            drawTile(x, y, tile);
        }
    }
}

export function drawHealthBar(token) {
    const healthBarHeight = 8;
    const healthBarOffset = 2;

    ctx.beginPath();
    ctx.rect(
        token.position.x - token.texture.w/2, 
        token.position.y - token.texture.h/2 - healthBarHeight - healthBarOffset, 
        token.texture.w, 
        healthBarHeight
    );
    ctx.fillStyle = 'gray';
    ctx.fill();

    ctx.beginPath();
    ctx.rect(
        token.position.x - token.texture.w/2, 
        token.position.y - token.texture.h/2 - healthBarHeight - healthBarOffset, 
        token.health.hp / token.health.maxHp * token.texture.w, 
        healthBarHeight
    );
    ctx.fillStyle = 'red';
    ctx.fill();
}

export function drawHud() {
    const offset = 10;
    const width = 96;
    const height = 16;

    /**Player Health */
    ctx.beginPath();
    ctx.rect(48, offset, width, height);
    ctx.fillStyle = 'gray';
    ctx.fill();

    ctx.beginPath();
    ctx.rect(48, offset, Player.health.hp / Player.health.maxHp * width, height);
    ctx.fillStyle = 'red';
    ctx.fill();

    /**Player Mana */
    ctx.beginPath();
    ctx.rect(192, offset, width, height);
    ctx.fillStyle = 'gray';
    ctx.fill();

    ctx.beginPath();
    ctx.rect(192, offset, Player.mana.mp / Player.mana.maxMp * width, height);
    ctx.fillStyle = 'blue';
    ctx.fill();

    /**Bomb Count */
    ctx.font = '24px serif';
    ctx.fillStyle = 'green';
    ctx.fillText(('Bombs x' + Player.bombs), 336, 24);

    ctx.font = '24px serif';
    ctx.fillStyle = 'yellow';
    ctx.fillText(('Gold x' + Player.gold), 480, 24);
}

export function sortTokens(tokenA, tokenB) {
    let positionA = tokenA.position.y + tokenA.texture.h/2;
    let positionB = tokenB.position.y + tokenB.texture.h/2;
    if (positionA > positionB) {
        return 1;
    } else if (positionA < positionB) {
        return -1;
    } else {
        return 0;
    }
}

export function moveCamera(x, y) {
    let cx = Math.round(x - CameraWidth/2);
    let cy = Math.round(y - CameraHeight/2);
    
    //left
    if (cx < 0) {
        cx = 0;
    }
    //top
    if (cy < 0) {
        cy = 0;
    }
    //right
    if (cx > getMapWidth(Settings.map)) {
        cx = getMapWidth(Settings.map)
    }
    //bottom 
    if (cy > getMapHeight(Settings.map)) {
        cy = getMapHeight(Settings.map)
    }
    ctx.translate(-cx, -cy);
}