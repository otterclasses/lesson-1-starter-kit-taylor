import { createBox, createGhost, createLadder, createZombie } from "../tokens"

var map = {
  width: 20,
  height: 20,
  tiles: [
    17, 1, 2, 3, 1, 2, 3, 1, 55, 56, 57, 58, 3, 1, 2, 3, 1, 2, 3, 19, 49, 33, 34, 35, 33, 34, 35, 33, 87, 88, 89, 90, 35, 33, 34, 35, 33, 34, 35, 51, 81, 210, 66, 67, 65, 66, 67, 65, 66, 67, 65, 66, 67, 65, 66, 67, 65, 66, 212, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 251, 251, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 252, 233, 234, 250, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 220, 235, 236, 237, 238, 218, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 220, 267, 268, 269, 270, 218, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 188, 271, 272, 186, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 187, 187, 99, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 83, 49, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 51, 81, 242, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 244, 83, 0, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 0
  ]
}

let level = {
  map: map,
  spawn: {
    x: 480,
    y: 140,
  },
  tokens: [
    createLadder(384, 400),
    //createBox(384, 240),
    createZombie(240, 360),
    createGhost(120, 360),
  ]
}

export default level;