import { createBox, createGhost, createLadder, createSkeleton, createZombie } from "../tokens"

var map = {
  width: 10,
  height: 10,
  tiles: [1, 2, 2, 2, 2, 0, 0, 0, 0, 0, 33, 34, 34, 34, 34, 83, 0, 0, 0, 0, 66, 66, 66, 66, 212, 83, 0, 0, 0, 0, 40, 40, 40, 81, 244, 83, 0, 0, 0, 0, 0, 18, 18, 85, 244, 84, 18, 18, 18, 0, 49, 50, 50, 50, 244, 50, 50, 50, 50, 51, 49, 66, 66, 66, 99, 66, 66, 66, 66, 51, 49, 99, 99, 99, 99, 99, 99, 99, 99, 51, 49, 99, 99, 99, 99, 99, 99, 99, 99, 51, 0, 9, 9, 9, 9, 9, 9, 9, 9, 0]
}


export let level = {
  map: map,
  spawn: {
    x: 48,
    y: 104,
  },
  tokens: [
    createLadder(384, 400),
    createBox(384, 240),
    createZombie(320, 360),
    createSkeleton(120, 360),
  ]
}

//Makes it so the export doesn't have a specific variable name tied to it. Limited to one per file.
export default level;