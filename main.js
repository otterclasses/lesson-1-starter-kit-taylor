import { keysDown, keysUp, keysUpReset, getController, onInit, mouse } from './input.js';
import { clamp } from './utils.js';
import { updateWeapon, createSlash, createBomb, createExplosion, createFirebolt } from './tokens.js';
import { drawToken, drawDebug, drawMap, drawHealthBar, drawHud, sortTokens, moveCamera } from './draw.js'
import { sensorCheck } from './collision.js';
import { isWalkable, movementCheck, mobPathing, projectilePathing, isInteractable } from './movement.js';
import { attack, firebolt, bonfire, mobProjectiles } from './combat';
import { Tokens, Settings, DEBUG, Player, Weapon, CameraWidth, CameraHeight, Levels} from './globals';
import { loadLevel } from './levels.js';

function updateTimers(dt) {
    /**Looping backwards to avoid skips from deletions */
    for (let i = Tokens.length - 1; i >= 0; i--) {
        if ('cooldown' in Tokens[i]) {
            Tokens[i].cooldown -= dt;
        }
        if ('behavior' in Tokens[i]) {
            Tokens[i].behavior.aggroCooldown -= dt;
        }
        if (!('duration' in Tokens[i])) {
            continue;
        } 
        Tokens[i].duration -= dt;
        if (Tokens[i].duration <= 0) {
            if (Tokens[i].texture.imageId == 'weapon_bomb') {
                let x = Tokens[i].position.x;
                let y = Tokens[i].position.y;
                explosion(x, y);
            }
            Tokens.splice(i, 1);
        }
    }
}

function explosion(x, y) {
    console.log('kaboom at ', x, y);
    let explosion = createExplosion(x, y);
    Tokens.push(explosion);
    attack(explosion, explosion)
}


function updateMovement(dt, controller) {
    let nextX = Player.position.x;
    let nextY = Player.position.y;

    if (controller != null) {
        // clamp makes sure it stays under magnitude 1
        const { x, y } = clamp(controller.axes[0], controller.axes[1], 1);
        if (Math.abs(y) > 0.2) {
            nextY = Player.position.y + Player.speed * y * dt;
            // determines if the player is going up or down
            if (Math.abs(y) > Math.abs(x)) {
                if (y < 0) {
                    Player.direction = 'up';
                } else {
                    Player.direction = 'down';
                }
            }
        }
        if (Math.abs(x) > 0.2) {
            nextX = Player.position.x + Player.speed * x * dt;
            Player.texture.faceRight = x >= 0;
            //determines if the player is going left or right
            if (Math.abs(y) < Math.abs(x)) {
                if (x > 0) {
                    Player.direction = 'right';
                } else {
                    Player.direction = 'left';
                }
            }
        }
    }

    // TODO make sure diaganol is not faster on keyboard
    if ('ArrowUp' in keysDown || 'w' in keysDown) {
        nextY = Player.position.y - Player.speed * dt;
        Player.direction = 'up';
    } 
    if ('ArrowDown' in keysDown || 's' in keysDown) {
        nextY = Player.position.y + Player.speed * dt;
        Player.direction = 'down';
    }
    if ('ArrowLeft' in keysDown || 'a' in keysDown) {
        nextX = Player.position.x - Player.speed * dt;
        Player.texture.faceRight = false;
        Player.direction = 'left';
    }
    if ('ArrowRight' in keysDown || 'd' in keysDown) {
        nextX = Player.position.x + Player.speed * dt;
        Player.texture.faceRight = true;
        Player.direction = 'right';
    }

    //Checks for obstacles when moving horizontally
    if (movementCheck(Player, nextX, Player.position.y)) {
        if (isWalkable(nextX, Player.position.y + Player.texture.h/2)) {
            Player.position.x = nextX;
        }
    }
    //Checks for obstacles when moving vertically
    if (movementCheck(Player, Player.position.x, nextY)) {
        if (isWalkable(Player.position.x, nextY + Player.texture.h/2)) {
             Player.position.y = nextY;
        }
    }

    // check if interacble
    if (isInteractable(Player.position.x, Player.position.y)) {
        console.log("YAY")
    }
}

function updateActions(dt, controller) {
    //Drops a bomb
    if (' ' in keysDown || controller?.buttons[0].pressed) {
        if (Settings.bombCooldown <= 0 && Player.bombs > 0) {
            let bomb = createBomb(Player.position.x, Player.position.y);
            Tokens.push(bomb);
            Player.bombs -= 1;
            Settings.bombCooldown = 0.5;
        }
    }

    updateWeapon();
    //attacks in the direction the player is facing
    if (mouse.click || controller?.buttons[5].pressed) { // r1
        if (Settings.weaponCooldown <= 0) {
            attack(Player, Weapon);
            Tokens.push(createSlash(Weapon.position.x, Weapon.position.y, Player.texture.faceRight));
            Settings.weaponCooldown = 0.5;
        }
    }

    //shoots a firebolt in the firection the player is facing
    if ('c' in keysDown || controller?.buttons[7].pressed) { //r2
        if (Settings.weaponCooldown <= 0 && Player.mana.mp >= 1) {
            let firebolt = createFirebolt(Player.position.x, Player.position.y, Player.texture.faceRight, Player.direction);
            Tokens.push(firebolt);
            Player.mana.mp -= 1;
            Settings.weaponCooldown = 0.5;
        }
    }
    firebolt(dt);
    bonfire();

}



function update(dt) {
    let controller = getController();
    updateMovement(dt, controller);
    sensorCheck(Player, Tokens);
    updateActions(dt, controller);

    mobPathing(dt);
    mobProjectiles();
    projectilePathing(dt);
    
    updateTimers(dt);
    Settings.bombCooldown -= dt;
    Settings.weaponCooldown -= dt;

    //keysUpReset();
}

/**
 * This function is called right after update is called.
 * It clears the screen, draws the background, then then
 * draws player.
 */
function render(dt) {
    // Clears the screen with white
	ctx.save();
    ctx.clearRect(0, 0, CameraWidth, CameraHeight);
    ctx.fillColor = "#000";
    ctx.fillRect(0, 0, CameraWidth, CameraHeight);
    ctx.imageSmoothingEnabled = false;

    moveCamera(Player.position.x, Player.position.y);

    drawMap(Settings.map);
    
    // stylin
    // better way --> tokens.sort((a,b) => a.x+a.h/2-b.x+b.h/2));
    Tokens.sort(sortTokens);

    // draws health bars for the loaded tokens
    for (let i = 0; i < Tokens.length; i++) {
        drawToken(Tokens[i], (Tokens[i].texture.faceRight != undefined) ? Tokens[i].texture.faceRight : false);
        if ('health' in Tokens[i] && Tokens[i] != Player) {
            drawHealthBar(Tokens[i]);
        }
    }
    
    // debug mode
    if (DEBUG) {
        for (let i = 0; i < Tokens.length; i++) {
            drawDebug(Tokens[i]);
        }
    }

	ctx.restore();
    ctx.save();
    drawHud();
    ctx.restore();
}


export function init() {
    loadLevel(Levels[Settings.curLevel]);
    let startTime = new Date();
    let endTime;
    onInit();
    setInterval(function() {
        endTime = new Date();
        const dt = (endTime - startTime) / 1000;
        startTime = new Date();
        update(dt);
        render(dt);
    }, 1000/60);
}