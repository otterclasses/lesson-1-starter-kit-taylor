import { updateHealth } from './combat';
import { SlashImg, Player, Weapon, Tokens } from './globals';
import { Position, Texture, CircleHitbox, Health, Mana, Loot, Behavior, Ranged, RectHitbox } from './components';
import { levelTransition } from './levels.js';

export function updateWeapon() {
    // if (Player.texture.faceRight) {
    //     Weapon.position.x = Player.position.x + 24;
    // } else {
    //     Weapon.position.x = Player.position.x - 24;
    // }

    // Weapon.position.y = Player.position.y - Weapon.texture.h / 4;

    if (Player.direction == 'up') {
        Weapon.position.x = Player.position.x;
        Weapon.position.y = Player.position.y - Player.texture.h/2 - 24;
    } else if (Player.direction == 'down') {
        Weapon.position.x = Player.position.x;
        Weapon.position.y = Player.position.y;
    } else if (Player.direction == 'right') {
        Weapon.position.x = Weapon.position.x = Player.position.x + 24;
        Weapon.position.y = Player.position.y - Weapon.texture.h / 4;
    } else {
        Weapon.position.x = Weapon.position.x = Player.position.x - 24;
        Weapon.position.y = Player.position.y - Weapon.texture.h / 4;
    }
}

export function createSlash(x, y, faceRight) {
    let slash =  {
        position: new Position( faceRight ? x + 16 : x - 16, y + 24),
        texture: new Texture("slash", 48, 62, SlashImg),
        duration: 0.032,
    }
    slash.texture.faceRight = faceRight;
    return slash;
}

export function createZombie(x, y) {
    return {
        type: 'zombie',
        position: new Position(x, y),
        texture: new Texture('monster_zombie', 48, 48),
        hitbox: new CircleHitbox(18),
        health: new Health(10),
        loot: new Loot(2, 0, 0, 1),
        behavior: new Behavior(96, 192),
        speed: 48,
        damage: 3,
        cooldown: 0,
    }
}

export function createSkeleton(x, y) {
    return {
        type: 'skeleton',
        position: new Position(x, y),
        texture: new Texture('monster_skelet', 48, 48),
        hitbox: new CircleHitbox(18),
        health: new Health(5),
        loot: new Loot(1, 0, 1, 1),
        behavior: new Behavior(144, 192),
        ranged: new Ranged('dagger', 48),
        speed: 96,
        damage: 2,
        cooldown: 0,
    }
}


export function createGhost(x, y) {
    return {
        type: 'ghost',
        position: new Position(x, y),
        texture: new Texture('monster_elemental_air', 48, 48),
        hitbox: new CircleHitbox(18),
        health: new Health(8),
        loot: new Loot(1, 1, 0, 0),
        behavior: new Behavior(144, 145),
        speed: 96,
        damage: 1,
        cooldown: 0,
        incorporeal: true,
    }
}

export function createOgre(x, y) {
    let ogre = {
        type: 'ogre',
        position: new Position(x, y),
        texture: new Texture('monster_ogre', 96, 96),
        hitbox: new CircleHitbox(36),
        health: new Health(30),
        loot: new Loot(1, 0, 0, 1),
        behavior: new Behavior(192, 288),
        ranged: new Ranged('hammer', 96),
        speed: 96,
        damage: 3,
        cooldown: 0,
    }
    return ogre;
}

export function createBox(x, y) {
    return {
        position: new Position(x, y + 24 * 2),
        texture: new Texture('box', 48, 96),
        hitbox: new RectHitbox(48, 48, 20),
    }
}

export function createLadder(x, y) {
    let ladder = {
        position: new Position(x, y),
        texture: new Texture('Floor_ladder', 48, 48),
        hitbox: new CircleHitbox(12),
    }
    ladder.hitbox.addSensor(ladderCollision);
    return ladder;
}

export function ladderCollision() {
    console.log("Going to next level");
    levelTransition();
}

export function createFirebolt(x, y, faceRight, direction) {
    let firebolt = {
        type: 'firebolt',
        position: new Position(x, y),
        texture: new Texture('monster_elemental_fire_small', 48, 48),
        hitbox: new CircleHitbox(24),
        speed: 240,
        duration: 2,
        damage: 2,
        incorporeal: true,
        direction: direction,
    }
    firebolt.texture.faceRight = faceRight;
    return firebolt;
}

export function createBonfire(x, y) {
    return {
        type: 'bonfire',
        position: new Position(x, y),
        texture: new Texture('monster_elemental_fire', 48, 48),
        hitbox: new CircleHitbox(24),
        duration: 3,
        damage: 1,
        cooldown: 0,
        incorporeal: true,

    }
}

export function createHammer(x, y, direction) {
    return {
        type: 'projectile',
        position: new Position(x, y),
        texture: new Texture('weapon_hammer', 24, 57),
        hitbox: new CircleHitbox(24),
        speed: 240,
        //duration: 1,
        damage: 2,
        incorporeal: true,
        direction: direction,
    }
}

export function createDagger(x, y, direction) {
    return {
        type: 'projectile',
        position: new Position(x, y),
        texture: new Texture('weapon_dagger_small', 24, 48),
        hitbox: new CircleHitbox(12),
        speed: 240,
        //duration: 1,
        damage: 2,
        incorporeal: true,
        direction: direction,
    }
}

export function createBomb(x, y) {
    return {
        position: new Position(x, y),
        texture: new Texture('weapon_bomb', 48, 48),
        duration: 2,
    }
}

export function createExplosion(x, y) {
    let explosion = {
        type: 'explosion',
        position: new Position(x, y),
        texture: new Texture('monster_tentackle', 96, 96),
        hitbox: new CircleHitbox(48),
        duration: 0.5,
        damage: 5,
    }
    explosion.hitbox.addSensor();
    return explosion;
}

export function createCorpse(x, y) {
    return {
        position: new Position(x, y),
        texture: new Texture('monster_elemental_goo_small', 48, 48),
        duration: 1,
    }
}

export function createLoot(x, y, imageId) {
    let loot = {
        type: 'loot',
        position: new Position(x, y),
        texture: new Texture(imageId, 27, 33),
        hitbox: new CircleHitbox(16, undefined, true),
        incorporeal: true,
    }
    loot.hitbox.addSensor(lootCollision);
    return loot;
}

export function lootCollision(loot, player) {
    console.log('loot is here');
    if (loot.texture.imageId == 'flask_red') {
        updateHealth(player, 3);
    } else if (loot.texture.imageId == 'flask_blue') {
        player.mana.mp += 3;
        if (player.mana.mp > player.mana.maxMp) {
            player.mana.mp = player.mana.maxMp
        }
    } else if (loot.texture.imageId == 'flask_green') {
        player.bombs += 1;
    } else if (loot.texture.imageId == 'flask_yellow') {
        player.gold += 1;
    }
    Tokens.splice(Tokens.indexOf(loot), 1);
}